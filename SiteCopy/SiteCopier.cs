﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CsQuery;
using NLog;

namespace SiteCopy
{
    public class SiteCopier
    {
        public string Url { get; protected set; }
        public string Folder { get; protected set; }

        private Queue<string> queueSites = new Queue<string>();

        private List<string> createdSites = new List<string>();

        private static Logger logger = LogManager.GetCurrentClassLogger();

        private string CurrentSite { get; set; }

        public SiteCopier(string url, string folder)
        {
            if (String.IsNullOrEmpty(url) || String.IsNullOrEmpty(folder))
            {
                throw new ArgumentException($"Parameter url is not valid");
            }

            Url = url;
            Folder = folder;

            queueSites.Enqueue(Url);
        }
        public async void GetPages()
        {
            HttpClient client = new HttpClient();
            HttpRequestMessage request = new HttpRequestMessage();
            CurrentSite = queueSites.Dequeue();
            request.RequestUri = new Uri(CurrentSite);
            request.Method = HttpMethod.Get;

            logger.Info($"Start Get request to {CurrentSite}");
            HttpResponseMessage response = await client.SendAsync(request);
            logger.Info($"Finish Get request to {CurrentSite}");

            if (response.StatusCode == HttpStatusCode.OK)
            {
                HttpContent responseContent = response.Content;
                var markup = await responseContent.ReadAsStringAsync();
                CreateSite(markup);
                ProcessLinks(markup);
            }
        }

        private void ProcessLinks(string html)
        {
            logger.Info($"Start process links on page {CurrentSite}");
            CQ cq = CQ.Create(html);
            foreach (IDomObject obj in cq.Find("a"))
            {
                queueSites.Enqueue(obj.GetAttribute("href"));
            }
        }

        private void CreateSite(string markup)
        {
            logger.Info($"Save in file {CurrentSite}");
            using (FileStream fs = new FileStream(Folder + "/main.html", FileMode.Create))
            {
                using (StreamWriter writer = new StreamWriter(fs, Encoding.UTF8))
                {
                    writer.WriteLine(markup);
                }
            }
        }

        public void ProcessImages(string html)
        {
            
        }

        private string GetRootSite()
        {
            return new Uri(Url).Host;
        }
    }
}
